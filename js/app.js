

angular.module('testApp', [])

.controller('appController', ['$scope', function($scope) {

	$scope.onEnter = function(){
		$scope.edit = true;
	};

    $scope.cardOptions = {
    	color: ["red", "green", "orange"],
    	avatar: ["images/image1.jpg", "images/image2.jpg", "images/image3.jpg", "images/image4.jpg", "images/image5.jpg"],
    	description: ["Improve contrast to make jared happy :)", "Header ilustration", "Icons on the dashboard", "Design nwe ilustrations for the app onboard screens using slack", "Slider animation", "Onboard screens", "Login error", "Create a new logo for app", "The account settings screen needs to be mocked up", "Replace dashboard icons with more coloful icons"]
    };

    $scope.ideasCards = [
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    }
    ];

    $scope.progressCards = [
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    }
    ];

    $scope.reviewCards = [
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    }
    ];

    $scope.approvedCards = [
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    },
    {
    	color: "red",
    	avatar: "images/image1.jpg",
    	description: "Improve contrast to make jared happy :)"
    }
    ];

    $scope.randomCards = function(loop){
    	var result = [];
    	for(i=0; i<loop; i++) {
    		var tempObj = {};
    		tempObj.color = $scope.cardOptions.color[Math.floor(Math.random() * $scope.cardOptions.color.length)];
    		tempObj.avatar = $scope.cardOptions.avatar[Math.floor(Math.random() * $scope.cardOptions.avatar.length)];
    		tempObj.description = $scope.cardOptions.description[Math.floor(Math.random() * $scope.cardOptions.description.length)];
    		result.push(tempObj);
    	}
    	return result;
    };

    $scope.addIdea = function() {
    	var newIdea = {};
    	newIdea.color = "green";
    	newIdea.avatar = "images/image2.jpg";
    	newIdea.description = $scope.idea;
    	$scope.ideasCards.push(newIdea);
    	$scope.idea = "";
    };
    $scope.addProgress = function() {
    	var newIdea = {};
    	newIdea.color = "orange";
    	newIdea.avatar = "images/image4.jpg";
    	newIdea.description = $scope.progress;
    	$scope.progressCards.push(newIdea);
    	$scope.progress = "";
    };
    $scope.addReview = function() {
    	var newIdea = {};
    	newIdea.color = "red";
    	newIdea.avatar = "images/image5.jpg";
    	newIdea.description = $scope.review;
    	$scope.reviewCards.push(newIdea);
    	$scope.review = "";
    };
    $scope.addApproved = function() {
    	var newIdea = {};
    	newIdea.color = "orange";
    	newIdea.avatar = "images/image3.jpg";
    	newIdea.description = $scope.approved;
    	$scope.approvedCards.push(newIdea);
    	$scope.approved = "";
    };

}])

